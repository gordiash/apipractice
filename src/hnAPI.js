export async function getUsers(){
    const result = await fetch("https://jsonplaceholder.typicode.com/users")
    const news = await result.json()

    return news
}

export async function getPosts(){
    const result = await fetch("https://jsonplaceholder.typicode.com/posts")
    const posts = await result.json()

    return posts
}