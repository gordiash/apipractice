import { getUsers } from "./hnAPI";

Promise.all([getUsers()]).then(data => {
  const users = data[0];

  for (let user of users) {
    const userDiv = `<div id="user" class="container" data-id='${user.id}'>
      <h3 class="is-size-2">${user.name}</h3>
      <button class="button is-primary">Show Posts</button>
      <div class="user-posts hide"></div>
    </div>`;

    document.getElementById("app").innerHTML += userDiv;

    const btns = document.querySelectorAll("button");

    btns.forEach(btn => {
      btn.addEventListener("click", e => {
        e.target.nextElementSibling.classList.toggle("show");
        
        if (e.target.nextElementSibling.classList.contains("show")) {
         
          let usid = e.target.parentElement.getAttribute("data-id");

          posts(usid).then(posts => {
            for (let post of posts) {
              const postDiv = `<div id="post" class="box" data-id='${post.id}'>
            <h3 class="title">${post.title}</h3>
            <p class="content has-text-info">${post.body}</p>
          </div>`;

              e.target.nextElementSibling.innerHTML += postDiv;
            }
          });
        } else {

              e.target.nextElementSibling.innerHTML = "";
        }
      });
    });
  }
});

async function posts(id) {
  const result = await fetch(
    `https://jsonplaceholder.typicode.com/posts?userId=${id}`
  );
  const posts = await result.json();

  return posts;
}
